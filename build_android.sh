#!/bin/sh
# ______________________________________________________________________________
#
#  Compile raylib project for Android
# ______________________________________________________________________________
#
ABIS="armeabi-v7a x86 x86_64"
ANDROID_SDK=$HOME/Development/Tools/android-sdk
COMMON_TOOLS=$HOME/Development/Tools/.install
BUILD_TOOLS=$ANDROID_SDK/build-tools/29.0.3
TOOLCHAIN=$ANDROID_SDK/android-ndk/toolchains/llvm/prebuilt/linux-x86_64
NATIVE_APP_GLUE=$ANDROID_SDK/android-ndk/sources/android/native_app_glue

FLAGS="-ffunction-sections -funwind-tables -fstack-protector-strong -fPIC -Wall \
	-Wformat -Werror=format-security -no-canonical-prefixes \
	-DANDROID -DPLATFORM_ANDROID -D__ANDROID_API__=29"

INCLUDES="-I. -I$HOME/Development/Tools/.install/inc -I../include -I$NATIVE_APP_GLUE -I$TOOLCHAIN/sysroot/usr/include"

set -e

# Copy icons
mkdir --parents build/res/drawable-ldpi
cp assets/icon_ldpi.png build/res/drawable-ldpi/icon.png

mkdir --parents build/res/drawable-mdpi
cp assets/icon_mdpi.png build/res/drawable-mdpi/icon.png

mkdir --parents build/res/drawable-hdpi
cp assets/icon_hdpi.png build/res/drawable-hdpi/icon.png

mkdir --parents build/res/drawable-xhdpi
cp assets/icon_xhdpi.png build/res/drawable-xhdpi/icon.png

# Copy other assets
mkdir --parents build/assets
cp assets/* build/assets

# ______________________________________________________________________________
#
#  Compile
# ______________________________________________________________________________
#
mkdir --parents build/obj
for ABI in $ABIS; do
	case "$ABI" in
		"armeabi-v7a")
			CCTYPE="armv7a-linux-androideabi"
			ABI_FLAGS="-std=c99 -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16"
			;;

		"arm64-v8a")
			CCTYPE="aarch64-linux-android"
			ABI_FLAGS="-std=c99 -target aarch64 -mfix-cortex-a53-835769"
			;;

		"x86")
			CCTYPE="i686-linux-android"
			ABI_FLAGS=""
			;;

		"x86_64")
			CCTYPE="x86_64-linux-android"
			ABI_FLAGS=""
			;;
	esac
	CC="$TOOLCHAIN/bin/${CCTYPE}29-clang"

   mkdir --parents build/lib/$ABI
	# Compile native app glue
	# .c -> .o
	$CC -c $NATIVE_APP_GLUE/android_native_app_glue.c -o $NATIVE_APP_GLUE/native_app_glue.o \
		$INCLUDES -I$TOOLCHAIN/sysroot/usr/include/$CCTYPE $FLAGS $ABI_FLAGS

	# .o -> .a
	$TOOLCHAIN/bin/llvm-ar rcs build/lib/$ABI/libnative_app_glue.a $NATIVE_APP_GLUE/native_app_glue.o

	# Compile project
	$CC src/*.c -o build/lib/$ABI/libmain.so -shared \
		$INCLUDES -I$TOOLCHAIN/sysroot/usr/include/$CCTYPE $FLAGS $ABI_FLAGS \
		-Wl,-soname,libmain.so -Wl,--exclude-libs,libatomic.a -Wl,--build-id \
		-Wl,--no-undefined -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now \
		-Wl,--warn-shared-textrel -Wl,--fatal-warnings -u ANativeActivity_onCreate \
		-L. -Lbuild/obj -L$COMMON_TOOLS -Lbuild/lib/$ABI \
		-lraylib-android-$ABI -lnative_app_glue -llog -landroid -lEGL -lGLESv2 -lOpenSLES -latomic -lc -lm -ldl
done

# ______________________________________________________________________________
#
#  Build APK
# ______________________________________________________________________________
#
mkdir --parents build/src
$BUILD_TOOLS/aapt package -f -m \
	-S build/res -J build/src -M AndroidManifest.xml \
	-I $ANDROID_SDK/platforms/android-29/android.jar

# Compile NativeLoader.java
mkdir --parents build/src/app/baremetalcord/bmc_scoreboards
javac -verbose -source 1.8 -target 1.8 -d build/obj \
	-bootclasspath jre/lib/rt.jar \
	-classpath $ANDROID_SDK/platforms/android-29/android.jar:build/obj \
	-sourcepath src build/src/app/baremetalcord/bmc_scoreboards/R.java \
	src/app/baremetalcord/bmc_scoreboards/NativeLoader.java

mkdir --parents build/dex
$BUILD_TOOLS/dx --verbose --dex --output=build/dex/classes.dex build/obj

# Add resources and assets to APK
$BUILD_TOOLS/aapt package -f \
	-M AndroidManifest.xml -S build/res -A assets \
	-I $ANDROID_SDK/platforms/android-29/android.jar -F build/bmc_scorebords.apk build/dex

# Add libraries to APK
pushd build
for ABI in $ABIS; do
	$BUILD_TOOLS/aapt add ../build/bmc_scorebords.apk lib/$ABI/libmain.so
done
popd

# Sign and zipalign APK
# NOTE: If you changed the storepass and keypass in the setup process, change them here too
jarsigner -keystore raylib.keystore -storepass raylib -keypass raylib \
	-signedjar build/bmc_scorebords.apk build/bmc_scorebords.apk projectKey

$BUILD_TOOLS/zipalign -f 4 build/bmc_scorebords.apk game.final.apk
mv -f game.final.apk build/bmc_scorebords.apk

# Install to device or emulator
$ANDROID_SDK/platform-tools/adb install -r build/bmc_scorebords.apk
# Run on device
$ANDROID_SDK/platform-tools/adb shell am start -n app.baremetalcord.bmc_scoreboards/app.baremetalcord.bmc_scoreboards.NativeLoader
