#ifndef BMC_SCOREBOARDS_DICE_ROLLS_H
#define BMC_SCOREBOARDS_DICE_ROLLS_H

#include <stdbool.h>

void dice_rolls_init(void);
bool dice_rolls_draw(float window_width, float window_height);

#endif//BMC_SCOREBOARDS_DICE_ROLLS_H
