#include <stdio.h>
#include <string.h>
#include "gui.h"
#include "dice_rolls.h"

typedef enum{
   MAIN_MENU,
   CREATE_PLAYERS,
   SCOREBOARD,
   SCOREBOARD_GAME
} BMCState;

typedef enum{
   CLOSE_GAME,
   SHOW_SCOREBOARD,
   REQUEST_COUNT
} BMCRequest;

typedef enum{
   DICE_ROLLS,
   SCOREBOARD_GAMES_COUNT,
   NO_SCOREBOARD = 0xFF
} ScoreboardGame;

typedef bool (*ScoreboardDraw)(float screen_width, float screen_height);
typedef void (*ScoreboardInit)(void);

typedef struct{
   ScoreboardDraw draw;
   ScoreboardInit init;
// ScoreboardDeinit deinit;
// ScoreboardNextPlayer next_player;
// ScoreboardShow scoreboard;
}ScoreboardAppIface;

const ScoreboardAppIface apps[SCOREBOARD_GAMES_COUNT] = {
   (ScoreboardAppIface){
      .draw = dice_rolls_draw,
      .init = dice_rolls_init
   }
};
const ScoreboardAppIface dice_rolls_export = {
 0  
};

// TODO define structure of standard needed calls
const ScoreboardDraw draw_calls[SCOREBOARD_GAMES_COUNT] = {
   dice_rolls_draw
};

const ScoreboardInit draw_inits[SCOREBOARD_GAMES_COUNT] = {
   dice_rolls_init
};

const char* scoreboard_names[SCOREBOARD_GAMES_COUNT] = {
   "Dice Rolls"
};


int main(void)
{
   const int screenHeight = 800;
   const int screenWidth = 450;

   const float game_button_base_x = 10.f;
   const float game_button_base_y = 30.f;
   const float game_button_padding_y = 5.f;
   const float game_button_width = screenWidth - 2*game_button_base_x;
   const float game_button_height = 40.f;

   InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

   SetTargetFPS(60);

   gui_init();
   ScoreboardGame current_game = NO_SCOREBOARD;
   Color background_color = GetColor(GuiGetStyle(DEFAULT,BACKGROUND_COLOR));

   while (!WindowShouldClose())
   {

      BeginDrawing();

      ClearBackground(background_color);

      if(current_game == NO_SCOREBOARD){
         GuiPanel((Rectangle){0,0,screenWidth,screenHeight},"Choose the game");

         for(size_t i = 0; i < SCOREBOARD_GAMES_COUNT; i++){
            if(GuiButton((Rectangle){
                     .x = game_button_base_x,
                     .y = game_button_base_y + i*(game_button_height+game_button_padding_y),
                     .width = game_button_width,
                     .height = game_button_height
                     },scoreboard_names[i])){
               draw_inits[i]();
               current_game = i;
            }
         }
      }
      else{
         if(draw_calls[current_game](screenWidth,screenHeight)) current_game = NO_SCOREBOARD;
      }

      EndDrawing();

   }

   CloseWindow();        

   return 0;
}
