#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
#include "style_cyber.h"

int GuiKeyboard(Rectangle bounds){
   int result = 0;
   GuiState state = guiState;
   static bool shift = false;
#define KEYBOARD_ROWS 4
#define KEYBOARD_COLS 10

#define KEY_WIDTH
#define KEY_HEIGHT
#define KEYBOARD_MARGIN 10

   const char keys[KEYBOARD_ROWS][KEYBOARD_COLS+1] = {
      "qwertyuiop",
      "asdfghjkl",
      "zxcvbnm",
   };

   // Update control
   //--------------------------------------------------------------------

   //--------------------------------------------------------------------

   // Draw control
   //--------------------------------------------------------------------
   float keys_start_x = (bounds.x + KEYBOARD_MARGIN);
   float key_height = (bounds.height - KEYBOARD_MARGIN)/KEYBOARD_ROWS - KEYBOARD_MARGIN;
   float key_width = (bounds.width - KEYBOARD_MARGIN)/KEYBOARD_COLS - KEYBOARD_MARGIN;
   
   // firstly provide separate panel with optional indication what it is
   GuiPanel(bounds, NULL);
   // next determine state of shift key
   GuiToggle((Rectangle){
            .x = keys_start_x,
            .y = bounds.y + KEYBOARD_MARGIN + 3*(key_height + KEYBOARD_MARGIN),
            .width = key_width,
            .height = key_height
            },"Shift",&shift);
   // next arrange buttons of querty keyboard (except last row which is for special keys)
   for(size_t i = 0; i < KEYBOARD_ROWS-1; i++){
      size_t keys_in_row = strlen(keys[i]);
      float row_start_x = keys_start_x + (KEYBOARD_COLS-keys_in_row)*(key_width + KEYBOARD_MARGIN)/2;
      for(size_t j = 0; j < keys_in_row; j++){
         char key_text[2] = {0};
         *key_text = keys[i][j];
         if(shift) *key_text -= 32; //? I'm guessing
         if(GuiButton((Rectangle){
               .x = row_start_x + j*(key_width + KEYBOARD_MARGIN),
               .y = bounds.y + KEYBOARD_MARGIN + i*(key_height + KEYBOARD_MARGIN),
               .width = key_width,
               .height = key_height
               }, key_text)) 
         // return key that was printed, because it was recalculated before
         result = result ? result : (int)*key_text;
      }
   }
   if(GuiButton((Rectangle){
            .x = keys_start_x + key_width + KEYBOARD_MARGIN,
            .y = bounds.y + KEYBOARD_MARGIN + 3*(key_height + KEYBOARD_MARGIN),
            .width = key_width + 7*(key_width + KEYBOARD_MARGIN),
            .height = key_height
            },"Spacebar")) 
      result = ' ';
   if(GuiButton((Rectangle){
            .x = keys_start_x + 9*(key_width + KEYBOARD_MARGIN),
            .y = bounds.y + KEYBOARD_MARGIN + 3*(key_height + KEYBOARD_MARGIN),
            .width = key_width,
            .height = key_height
            },"Bcsp")) 
      // escape key for backspace
      result = '\b';

   //------------------------------------------------------------------

   return result;
}

void gui_init(){
   GuiLoadStyleCyber(); 
}
