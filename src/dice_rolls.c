#include "dice_rolls.h"
#include <stdio.h>
#include <string.h>

#include "gui.h"

#define SCOREBOARD_PER_PLAYER_SIZE 15
#define SCOREBOARD_SCHOOL_SCORE_COUNT 6
#define SCOREBOARD_MAX_PLAYERS 18
#define PLAYER_MAX_SIZE 10
#define DICE_COUNT 5


typedef enum{
   DICE_CREATE_PLAYERS,
   DICE_NEXT_PLAYER,
   DICE_TOSS,
   DICE_SELECT_SCORE,
   DICE_SCOREBOARD,
   DICE_GAME_OVER
}DiceGameState;

typedef enum{
   ONES,
   TWOS,
   THREES,
   FOURS,
   FIVES,
   SIXES,
   PAIR,
   DOUBLE_PAIR,
   THIRD,
   SMALL_STRAIGHT,
   LARGE_STRAIGHT,
   FULL_HOUSE,
   CARRETTE,
   CHANCE,
   POKER,
} ScoreType;

typedef struct{
   bool filled;
   int score;
} DiceRollScore;

const char* score_type[SCOREBOARD_PER_PLAYER_SIZE] = {
   "1",
   "2",
   "3",
   "4",
   "5",
   "6",
   "Pair",
   "Double Pair",
   "Third",
   "Small Straight",
   "Large Straight",
   "Full House",
   "Carrette",
   "Chance",
   "Poker",
};
const float score_type_base_position_x = 10.f;
const float score_type_base_position_y = 50.f;
const float score_type_height = 20.f;

static char players[SCOREBOARD_MAX_PLAYERS][PLAYER_MAX_SIZE+1] = {0};
static size_t player_count = 0;
const float players_base_position_x = 110.f;
const float players_base_position_y = 20.f;

static DiceRollScore scoreboard[SCOREBOARD_MAX_PLAYERS][SCOREBOARD_PER_PLAYER_SIZE] = {0};
const float scoreboard_base_position_x = 110.f;
const float scoreboard_base_position_y = 40.f;
const float score_height = 35.f;
const float score_width =  100.f;
const float score_padding_x = 10.f;
const float score_padding_y = 10.f;
#define SCORE_GRID(a,b)\
   .x = scoreboard_base_position_x + (a)*(score_width + score_padding_x),\
   .y = scoreboard_base_position_y + (b)*(score_height+score_padding_y)

#define FULLSCREEN (Rectangle){0, 0, window_width, window_height}

#define SET_SCORE(scoreboad,player,score_type,value) \
   if(!scoreboard[player][score_type].filled) scoreboard[player][score_type].score = value

const float input_margin_x = 5.f, input_margin_y = 5.f;
static int dice_values[DICE_COUNT] = {0};
static bool reserved_dices[DICE_COUNT] = {0};
static int selected_input = -1;

static DiceGameState state = DICE_CREATE_PLAYERS;
static size_t queue_number = 0;
static size_t player_number = 0;
static size_t toss_number = 1;

static Color background_color = {0};

static int winner = -1;

void dice_rolls_init(void){
   for(size_t i = 0; i < DICE_COUNT; i++){
      dice_values[i] = 0;
      reserved_dices[i] = 0;
   }

   for(size_t i = 0; i < SCOREBOARD_MAX_PLAYERS; i++){
      for(size_t j = 0; j < PLAYER_MAX_SIZE; j++) players[i][j] = 0;
      for(size_t j = 0; j < SCOREBOARD_PER_PLAYER_SIZE; j++) scoreboard[i][j] = (DiceRollScore){0};
   }

   background_color = GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR));
   player_count = 1;
   selected_input = -1;
   state = DICE_CREATE_PLAYERS;
   queue_number = 0;
   player_number = 0;
   toss_number = 1;

   winner = -1;
}

bool dice_are_all_filled(){
   bool result = true;

   for(size_t i = 0; i < DICE_COUNT; i++){
      // out of range check
      if(dice_values[i] < 1 || dice_values[i] > 6){
         result = false;
         break;
      }
   }

   return result;
}

void clear_after_round(size_t player){
   toss_number = 1;
   for(size_t i = 0; i < DICE_COUNT; i++){
      reserved_dices[i] = false;
      dice_values[i] = 0;
   }
   for(size_t i = 0; i < SCOREBOARD_PER_PLAYER_SIZE; i++){
      if(!scoreboard[player][i].filled){
         scoreboard[player][i].score = 0;
      }
   }
}

void dice_rolls_recalculate(){
   // scoreboard fill begin
   // calculate dice occurences
   size_t dice_occurences[6] = {0};
   for(size_t dice_value = 6; dice_value > 0; dice_value--){
      for(size_t i = 0; i < DICE_COUNT; i++){
         if(dice_values[i] == dice_value){
            dice_occurences[dice_value-1]++;
         }
      }
   }
   //ONES to SIXES
   for(ScoreType score = ONES; score <= SIXES; score++){
      SET_SCORE(scoreboard,player_number,score,(dice_occurences[score]-3)*(score+1));
   }
   //PAIR
   for(int dice = SIXES; dice >= ONES; dice--){
      if(dice_occurences[dice] >= 2){
         SET_SCORE(scoreboard,player_number,PAIR,(dice+1)*2);
         break;
      }
   }
   //DOUBLE PAIR
   size_t equal_dices[2] = {0};
   size_t pair_count = 0;
   for(int dice = SIXES; dice >= ONES; dice--){
      if(!pair_count && dice_occurences[dice] >=4){
         equal_dices[0] = dice+1;
         equal_dices[1] = dice+1;
         pair_count = 2;
      }
      else if(dice_occurences[dice] >= 2){
         equal_dices[pair_count++] = dice+1;
      }
      else;

      if(pair_count == 2){
         SET_SCORE(scoreboard,player_number,DOUBLE_PAIR,(equal_dices[0]+equal_dices[1])*2);
         break;
      }
   }
   //THIRD
   for(int dice = SIXES; dice >= ONES; dice--){
      if(dice_occurences[dice] >= 3){
         SET_SCORE(scoreboard,player_number,THIRD,(dice+1)*3);
         break;
      }
   }
   //FULL_HOUSE
   size_t third_dice = 0;
   size_t pair_dice = 0;
   for(int dice = SIXES; dice >= ONES; dice--){
      if(!pair_dice && !third_dice && dice_occurences[dice] == 5){
         third_dice = dice+1;
         pair_dice = dice+1;
      }
      else if(!third_dice && dice_occurences[dice] >= 3){
         third_dice = dice+1;
      }
      else if(!pair_dice && dice_occurences[dice] == 2){
         pair_dice = dice+1;
      }
      else;

      if(third_dice && pair_dice){
         SET_SCORE(scoreboard,player_number,FULL_HOUSE,third_dice*3 + pair_dice*2);
         break;
      }
   }
   //CARRETTE
   for(int dice = SIXES; dice >= ONES; dice--){
      if(dice_occurences[dice] >= 4){
         SET_SCORE(scoreboard,player_number,CARRETTE,(dice+1)*4);
         break;
      }
   }
   //POKER
   for(int dice = SIXES; dice >= ONES; dice--){
      if(dice_occurences[dice] == 5){
         SET_SCORE(scoreboard,player_number,POKER,50+(dice+1)*5);
         break;
      }
   }
   //SMALL_STRAIGHT
   if(
         dice_occurences[ONES] == 1 &&
         dice_occurences[TWOS] == 1 &&
         dice_occurences[THREES] == 1 &&
         dice_occurences[FOURS] == 1 &&
         dice_occurences[FIVES] == 1
     ){
      SET_SCORE(scoreboard,player_number,SMALL_STRAIGHT,15);
   }
   //LARGE_STRAIGHT
   if(
         dice_occurences[SIXES] == 1 &&
         dice_occurences[TWOS] == 1 &&
         dice_occurences[THREES] == 1 &&
         dice_occurences[FOURS] == 1 &&
         dice_occurences[FIVES] == 1
     ){
      SET_SCORE(scoreboard,player_number,LARGE_STRAIGHT,20);
   }
   //CHANCE
   size_t chance = 0;
   for(size_t i = ONES; i < SIXES; i++){
      chance += dice_occurences[i]*(i+1);
   }
   SET_SCORE(scoreboard,player_number,CHANCE,chance);
   //scoreboard fill end
}

int calculcate_sum(size_t player){
   int sum = 0;
   int i = 0;
   for(;i < SCOREBOARD_SCHOOL_SCORE_COUNT; i++){
      sum += scoreboard[player][i].filled ? scoreboard[player][i].score : 0;
   }
   if(sum < 0) sum -= 50;
   for(;i < SCOREBOARD_PER_PLAYER_SIZE; i++){
      sum += scoreboard[player][i].filled ? scoreboard[player][i].score : 0;
   }
   return sum;
}

static bool draw_create_players(DiceGameState *state, const float window_width, const float window_height){
   bool close_condition = false;

   size_t i = 0;
   size_t j = 0;
   
   static int player_selected = -1;
   static bool unique_players = true;
   static bool players_too_short = false;
   
   const float keygoard_pos_y = 500.f;
   const Rectangle keyboard_boundary = (Rectangle){0,keygoard_pos_y,window_width,window_height-keygoard_pos_y};
   
   if(!unique_players){
      if(-1 < GuiMessageBox(FULLSCREEN, "ERROR", "There are players of the same name, please correct it", "GotIt!"))
         unique_players = true;
   }
   else if (players_too_short){
      if(-1 < GuiMessageBox(FULLSCREEN, "ERROR", "Player name shall not be less than 3 characters", "GotIt!"))
         players_too_short = false;
   }
   else{
      bool delete = false;
      char current_key = GuiKeyboard(keyboard_boundary);

      const float player_base_x = score_type_base_position_x;
      const float player_base_y = score_type_base_position_y + 2*(score_height+score_padding_y);
      const float delete_button_width = score_height-9;
      size_t row_idx = -1;
      for(; i < player_count;){
         for(j = 0; j < 3 && i < player_count; j++, i++){
            row_idx += i%3 == 0;
            float player_shift_x = j*(score_width + delete_button_width + 2*score_padding_y);
            float player_shift_y = (score_height+score_padding_y)*row_idx;
            Rectangle player_textbox_boundary = (Rectangle){
                  .x = player_base_x + player_shift_x,
                  .y = player_base_y + player_shift_y,
                  .width = score_width,
                  .height = score_height
            };
            Rectangle player_remove_boundary = (Rectangle){
                  .x = player_base_x + player_shift_x + score_width + score_padding_x,
                  .y = player_base_y + player_shift_y,
                  .width = delete_button_width,
                  .height = score_height
            };
            if(GuiTextBox(player_textbox_boundary,players[i],PLAYER_MAX_SIZE,false)) player_selected = i;
            if(GuiButton(player_remove_boundary,"X")) { delete = true; break; }
            if(player_selected == i && current_key){
               size_t current_length = strlen(players[i]);
               if(current_key == '\b'){
                  if(current_length){
                     players[i][--current_length] = 0;
                  }
               }
               else{
                  if(current_length < PLAYER_MAX_SIZE){
                     players[i][current_length] = current_key;
                  }
               }
            }
         }
      }
      if(delete){
         for(++i; i < player_count; i++){
            memcpy(players[i-1],players[i],PLAYER_MAX_SIZE);
         }
         for(size_t j = 0; players[i-1][j]; j++) players[i-1][j] = 0;
         player_count--;
      } 
      else{
         // compensate for j and row_idx after leaving list printing loop
         if(j == 3) {
            j = 0;
            row_idx += i%3 == 0;
         }
         float player_shift_x = j*(score_width + delete_button_width + 2*score_padding_y);
         float player_shift_y = (score_height+score_padding_y)*row_idx;
         Rectangle add_player_boundary = (Rectangle){
               .x = player_base_x + player_shift_x,
               .y = player_base_y + player_shift_y,
               .width = score_width,
               .height = score_height
         };
         Rectangle start_game_boundary = (Rectangle){
               .x = scoreboard_base_position_x + 2*(score_width + score_padding_x),
               .y = scoreboard_base_position_y,
               .width = score_width,
               .height = score_height
         };
         if(i < SCOREBOARD_MAX_PLAYERS && GuiButton(add_player_boundary,"Add player")) player_count++;
         if(GuiButton(start_game_boundary,"Start Game")) {
            // check for unique player names
            for(size_t i = 0; i < player_count;i++){
               if(strlen(players[i]) < 3) {players_too_short = true; goto break_check;}
               for(size_t j = i+1; j < player_count; j++)
                  if(!strcmp(players[i],players[j])) {unique_players = false; goto break_check;}
            }
break_check:

            if(unique_players && ! players_too_short){
               player_selected = -1;
               *state = DICE_NEXT_PLAYER;
            }
         }
         Rectangle exit_game_boundary = (Rectangle){
               .x = scoreboard_base_position_x + 2*(score_width + score_padding_x),
               .y = scoreboard_base_position_y + score_height + score_padding_y,
               .width = score_width,
               .height = score_height
         };
         if(GuiButton(exit_game_boundary,"Exit Game")) close_condition = true;
      }
   }
   return close_condition;
}

static bool draw_next_player(DiceGameState *state, const float window_width, const float window_height){
   bool close_condition = false;
   char next_player_message[100] = {0};
   sprintf(next_player_message,"Now it's turn of %s.",players[player_number]);
   int mbox_return = GuiMessageBox(FULLSCREEN, "Next Player", next_player_message, "GotIt!;Exit_Game");
   if(1 == mbox_return){
      *state = DICE_TOSS;
   }
   else if(2 == mbox_return){
      close_condition = true;
   }
   else {}
   return close_condition;
}

static bool draw_toss(DiceGameState *state, const float window_width, const float window_height){
   bool close_condition = false;
   static int dice_toss_selected = -1;
   const char* dice_options = "Pick;1;2;3;4;5;6";
   Rectangle toss_boundary = (Rectangle){
         SCORE_GRID(2,12),
         .width = score_width,
         .height = score_height
   };
   if(GuiButton(toss_boundary, "Toss") && dice_are_all_filled()){
      dice_toss_selected = -1;
      *state = DICE_SELECT_SCORE;
   }
   else{
      Rectangle toss_text_boundary = (Rectangle){
            .x = scoreboard_base_position_x + 2*(score_width+score_padding_x),
            .y = scoreboard_base_position_y,
            .width = score_width,
            .height = score_height
      };
      GuiLabel(toss_text_boundary,"Toss");
      if(dice_toss_selected == -1){
         for(int i = DICE_COUNT-1; i >= 0; i--){
            Rectangle dice_boundary = (Rectangle){
                  SCORE_GRID(2,i+1),
                  .width = score_width,
                  .height = score_height
            };
            if(!reserved_dices[i] && GuiDropdownBox(dice_boundary,dice_options,&dice_values[i], false)){
               dice_toss_selected = i;
            }
         } 
      }
      else{
         Rectangle dice_boundary = (Rectangle){
               SCORE_GRID(2,dice_toss_selected+1),
               .width = score_width,
               .height = score_height
         };
         if(GuiDropdownBox(dice_boundary,dice_options,&dice_values[dice_toss_selected],true)){
            dice_toss_selected = -1;
         }
      }
   }
   return close_condition;
}

static bool draw_select_score(DiceGameState *state, const float window_width, const float window_height){
   bool close_condition = false;
   dice_rolls_recalculate();
   // render labels of score types
   for(int i = 0; i < SCOREBOARD_PER_PLAYER_SIZE; ++i){
      Rectangle score_type_boundary = (Rectangle){
            score_type_base_position_x,
            score_type_base_position_y + i*(score_height+score_padding_y),
            score_width,
            score_type_height
      };
      GuiLabel(score_type_boundary, score_type[i]);
   }

   // render score buttons
   for(int j = 0; j < SCOREBOARD_PER_PLAYER_SIZE; ++j){
      DiceRollScore* current = &scoreboard[player_number][j];
      char button_text[4] = {0};
      sprintf(button_text,"%d",current->score);
      Rectangle score_placement = {
         SCORE_GRID(0,j),
         .width = score_width,
         .height = score_height
      };
      if(!current->filled){
         if(!(j >= SCOREBOARD_SCHOOL_SCORE_COUNT && queue_number < 3)){
            if(GuiButton(score_placement, button_text)){
               current->filled = true;
               if(++player_number == player_count){
                  if(++queue_number == SCOREBOARD_PER_PLAYER_SIZE){
                     winner = 0;
                     int max_sum = calculcate_sum(0);
                     for(size_t i = 1; i < player_count; i++){
                        int sum = calculcate_sum(i);
                        if(sum > max_sum){
                           max_sum = sum;
                           winner = i;
                        }
                     }
                     *state = DICE_GAME_OVER;
                     break;
                  }
                  player_number = 0;
               }
               clear_after_round(player_number);
               *state = DICE_NEXT_PLAYER;
            }
         }
         else{
            GuiLabel(score_placement, button_text);
         }
      }
      else{
         GuiLabel(score_placement, button_text);
      }
   }

   Rectangle reserve_boundary = (Rectangle){SCORE_GRID(2,0),.width = score_width,.height = score_height};
   GuiLabel(reserve_boundary,"Reserve");

   for(size_t i = 0; i < DICE_COUNT; i++){
      char value[2] = {0};
      value[0] = dice_values[i]+'0';
      Rectangle reserve_dice_boundary = (Rectangle){
            SCORE_GRID(2,i+1),
            .width = score_width,
            .height = score_height
      };
      GuiToggle(reserve_dice_boundary,value,&reserved_dices[i]);

   } 
   
   Rectangle another_toss_boundary = (Rectangle){
         SCORE_GRID(2,12), 
         .width = score_width,
         .height = score_height
   };
   if(toss_number < 3 && GuiButton(another_toss_boundary, "Another Toss")){
      for(size_t i = 0; i < DICE_COUNT; i++){
         dice_values[i] = !reserved_dices[i] ? 0 : dice_values[i];
      }
      toss_number++;
      *state = DICE_TOSS;
   }

   Rectangle scoreboard_boundary = (Rectangle){
         SCORE_GRID(2,13), 
         .width = score_width,
         .height = score_height
   };
   if(GuiButton(scoreboard_boundary, "Scoreboard"))
      *state = DICE_SCOREBOARD;

   Rectangle exit_game_boundary = (Rectangle){
         SCORE_GRID(2,14), 
         .width = score_width,
         .height = score_height
   };
   if(GuiButton(exit_game_boundary,"Exit Game")) 
      close_condition = true;

   return close_condition;
}

// TODO make API for common scoreboard rendering
static bool draw_scoreboard(DiceGameState *state, const float window_width, const float window_height){
   bool close_condition = false;
   static float scroll_delta = 0.f;
   const int max_player_columns = 3;
   const float scroll_range_up = player_count > max_player_columns ? (player_count-max_player_columns)*(score_width+score_padding_x) : 0;

   // detect touch drag and calculate scroll position
   const float no_drag_pos = -1.f;
   static float drag_pos_start = no_drag_pos;
   static float drag_value = 0.f;
   Vector2 current_mouse_pos = GetMousePosition();
   if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)){
      if(drag_pos_start == no_drag_pos) drag_pos_start = current_mouse_pos.x;

      // calculate and clamp temporary scroll delta to fit table content
      drag_value = current_mouse_pos.x - drag_pos_start;
      if(scroll_delta+drag_value >= 0.f) drag_value = - scroll_delta;
      if(scroll_delta+drag_value <= -scroll_range_up) drag_value = - scroll_range_up - scroll_delta;
   }
   else{
      // apply currrent scroll delta and reset drag detection
      scroll_delta += drag_value;
      drag_value = 0.f;
      drag_pos_start = no_drag_pos;
   }

   for(int i = 0; i < player_count; ++i){
      int sum = calculcate_sum(i);
      char sum_str[5] = {0};

      Rectangle player_boundary = (Rectangle){
            .x = players_base_position_x + i*(score_width+score_padding_x) + scroll_delta + drag_value,
            .y = players_base_position_y,
            .width = score_width,
            .height = score_type_height
      };
      GuiLabel(player_boundary,players[i]);

      for(int j = 0; j < SCOREBOARD_PER_PLAYER_SIZE; ++j){
         int score = scoreboard[i][j].filled ? scoreboard[i][j].score : 0;
         char score_str[4] = {0};
         if(scoreboard[i][j].filled){
            sprintf(score_str,"%d",score);
         }
         else{
            *score_str = '-';
         }
         Rectangle score_boundary = (Rectangle){
               SCORE_GRID(i,j),
               .width = score_width,
               .height = score_height
         };
         score_boundary.x += scroll_delta + drag_value;
         GuiTextBox(score_boundary,score_str,3,false);
      }
      sprintf(sum_str,"%d",sum);
      Rectangle sum_boundary = (Rectangle){
            SCORE_GRID(i,SCOREBOARD_PER_PLAYER_SIZE),
            .width = score_width,
            .height = score_height
      };
      sum_boundary.x += scroll_delta + drag_value;
      GuiTextBox(sum_boundary,sum_str,4,false);
   }
   DrawRectangle(0,gui_panel_height,score_type_base_position_x+score_width,window_height-gui_panel_height,background_color);
   // render labels of score types
   for(int i = 0; i < SCOREBOARD_PER_PLAYER_SIZE; ++i){
      Rectangle score_type_boundary = (Rectangle){
            score_type_base_position_x,
            score_type_base_position_y + i*(score_height+score_padding_y),
            score_width,
            score_type_height
      };
      GuiLabel(score_type_boundary, score_type[i]);
   }
   Rectangle sum_boundary = (Rectangle){
         score_type_base_position_x,
         score_type_base_position_y + SCOREBOARD_PER_PLAYER_SIZE*(score_height+score_padding_y),
         score_width,
         score_type_height
   };
   GuiLabel(sum_boundary, "Sum");
   Rectangle return_boundary = (Rectangle){
         score_type_base_position_x,
         scoreboard_base_position_y + (SCOREBOARD_PER_PLAYER_SIZE+1)*(score_height+score_padding_y),
         score_width,
         score_height
   };
   if(GuiButton(return_boundary, "Return")){
      if(winner == -1){
         *state = DICE_SELECT_SCORE;
      }
      else{
         dice_rolls_init();
      }
   }
   return close_condition;
}

static bool draw_game_over(DiceGameState *state, const float window_width, const float window_height){
   bool close_condition = false;
   char game_over_message[126] = {0};
   sprintf(game_over_message,"The winner is %s. Congratulatons!",players[winner]);
   if(-1 < GuiMessageBox(FULLSCREEN, "ERROR", game_over_message, "See Scoreboard"))
      *state = DICE_SCOREBOARD;

   return close_condition;
}

bool dice_rolls_draw(const float window_width, const float window_height){
   bool close_condition = false;
   char panel_title[100] = {0};
   switch (state) {
      case DICE_CREATE_PLAYERS:
         sprintf(panel_title,"Create players");
         break;
      case DICE_TOSS:
         sprintf(panel_title,"%s: Toss",players[player_number]);
         break;
      case DICE_SELECT_SCORE:
         sprintf(panel_title,"%s: Select score and reserve dices.",players[player_number]);
         break;
      case DICE_SCOREBOARD:
         sprintf(panel_title,"Scoreboard");
         break;
      case DICE_GAME_OVER:
         sprintf(panel_title,"Game Over");
         break;
      default:
         break;
   }
   if(state != DICE_NEXT_PLAYER){
      GuiPanel(FULLSCREEN,panel_title);
   }

   switch (state) {
      case DICE_CREATE_PLAYERS:
         close_condition = draw_create_players(&state, window_width, window_height);
         break;
      case DICE_NEXT_PLAYER:
         close_condition = draw_next_player(&state, window_width, window_height);
         break;
      case DICE_TOSS:
         close_condition = draw_toss(&state, window_width, window_height);
         break;
      case DICE_SELECT_SCORE:
         close_condition = draw_select_score(&state, window_width, window_height);
         break;
      case DICE_SCOREBOARD:
         close_condition = draw_scoreboard(&state, window_width, window_height);
         break;
      case DICE_GAME_OVER:
         close_condition = draw_game_over(&state, window_width, window_height);
         break;
   }

   return close_condition;
}
