COMMON_TOOLS=$HOME/Development/Tools/.install

set -e

mkdir --parents build
clang src/*.c -o build/bmc_scoreboards -I$COMMON_TOOLS/inc \
      -Wl,--exclude-libs,libatomic.a -Wl,--build-id \
		-Wl,--no-undefined -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now \
		-Wl,--warn-shared-textrel -Wl,--fatal-warnings \
		-L. -L$COMMON_TOOLS \
		-lraylib -lm
./build/bmc_scoreboards
